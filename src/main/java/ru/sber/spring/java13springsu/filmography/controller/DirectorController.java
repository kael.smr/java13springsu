package ru.sber.spring.java13springsu.filmography.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.model.Director;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;
import ru.sber.spring.java13springsu.filmography.repository.FilmRepository;




@RestController
@RequestMapping(value = "/directors")
//http://localhost:9090/books
@Tag(name = "Директор",
        description = "контроллер для работы с режиссерами фильмов")
public class DirectorController extends GenericController<Director> {
    private final DirectorRepository directorRepository;

    private final FilmRepository filmRepository;

    public DirectorController(DirectorRepository directorRepository, FilmRepository filmRepository){
        super(directorRepository);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
    }

    @Operation(description = "Добавить фильм режиссеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> addFilm(@RequestParam(value = "filmId") Long filmId,
                                          @RequestParam(value = "directorId") Long directorId){
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("режиссер не найден"));
        director.getFilms().add(film);
        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(director));
    }


}
