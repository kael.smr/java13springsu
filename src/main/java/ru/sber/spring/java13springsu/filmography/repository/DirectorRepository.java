package ru.sber.spring.java13springsu.filmography.repository;


import ru.sber.spring.java13springsu.filmography.model.Director;


public interface DirectorRepository extends GenericRepository<Director> {
}
