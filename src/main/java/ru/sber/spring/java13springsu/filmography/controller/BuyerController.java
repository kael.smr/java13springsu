package ru.sber.spring.java13springsu.filmography.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.model.Buyer;
import ru.sber.spring.java13springsu.filmography.repository.BuyerRepository;

@RestController
@RequestMapping(value = "/buyers")
@Tag(name = "Пользователи",
description = "контроллер для работы с пользователями фильмов")
public class BuyerController extends GenericController<Buyer> {

    private final BuyerRepository buyerRepository;

    public BuyerController(BuyerRepository buyerRepository){
        super(buyerRepository);
        this.buyerRepository = buyerRepository;
    }
}
