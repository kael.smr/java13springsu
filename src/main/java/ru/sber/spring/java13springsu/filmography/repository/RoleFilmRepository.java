package ru.sber.spring.java13springsu.filmography.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.filmography.model.RoleFilm;

@Repository
public interface RoleFilmRepository extends GenericRepository<RoleFilm> {
}
