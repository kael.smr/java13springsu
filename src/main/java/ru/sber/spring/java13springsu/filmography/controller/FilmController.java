package ru.sber.spring.java13springsu.filmography.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.filmography.model.Director;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;
import ru.sber.spring.java13springsu.filmography.repository.FilmRepository;


@RestController
@RequestMapping(value = "/films")
//http://localhost:9090/books
@Tag(name = "Фильмы",
description = "контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film> {

    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;

    public FilmController(FilmRepository filmRepository, DirectorRepository directorRepository){
        super(filmRepository);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
    }

    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> addDirector(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId){
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("режиссер не найден"));
        film.getDirectors().add(director);
        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(film));
    }

}
