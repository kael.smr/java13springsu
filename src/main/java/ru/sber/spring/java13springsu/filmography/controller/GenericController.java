package ru.sber.spring.java13springsu.filmography.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.GenericRepository;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public abstract class GenericController<T extends GenericModel> {

    private final GenericRepository<T>  genericRepository;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericController(GenericRepository<T> genericRepository){
        this.genericRepository = genericRepository;
    }
    @Operation(description = "Получить информацию по ID", method = "getByID")
    @RequestMapping(value = "getById",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> getBookById(@RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(genericRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Данных нет по заданному ID " + id)));
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <List<T>> getAll() { return ResponseEntity.status(HttpStatus.OK).body(genericRepository.findAll());}


    @Operation(description = "Создать", method = "create")
    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> create(@RequestBody T newEntity){
        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(genericRepository.save(newEntity));
    }

    @Operation(description = "Обновить", method = "update")
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> update(@RequestBody T updatedEntity, @RequestParam(value = "id") Long id){
        updatedEntity.setId(id);
        updatedEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(genericRepository.save(updatedEntity));
    }

    @Operation(description = "Удалить", method = "delete")
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE)
    void delete(@PathVariable(value = "id") Long id){
        genericRepository.deleteById(id);

    }
}
