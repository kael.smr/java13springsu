package ru.sber.spring.java13springsu.filmography.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.model.Order;
import ru.sber.spring.java13springsu.filmography.repository.OrderRepository;

@RestController
@RequestMapping(name = "/orders")
@Tag(name = "Заказы",
description = "контроллер для работы с заказами на фильмы")
public class OrderController extends GenericController<Order>{
    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository){
        super(orderRepository);
        this.orderRepository = orderRepository;
    }
}
