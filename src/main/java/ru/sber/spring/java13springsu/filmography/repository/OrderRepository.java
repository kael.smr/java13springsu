package ru.sber.spring.java13springsu.filmography.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.filmography.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
}
