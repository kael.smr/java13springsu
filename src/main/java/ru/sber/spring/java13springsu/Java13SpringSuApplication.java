package ru.sber.spring.java13springsu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.sber.spring.java13springsu.dbexample.MyDbApplicationContext;
import ru.sber.spring.java13springsu.dbexample.constants.UserConst;
import ru.sber.spring.java13springsu.dbexample.dao.BookDaoBean;

import ru.sber.spring.java13springsu.dbexample.dao.BookDaoJDBC;
import ru.sber.spring.java13springsu.dbexample.dao.UserDaoBean;
import ru.sber.spring.java13springsu.dbexample.model.Book;
import ru.sber.spring.java13springsu.dbexample.model.Clients;

import java.sql.SQLException;
import java.util.Calendar;
import java.sql.Date;
import java.util.List;

@SpringBootApplication
public class Java13SpringSuApplication implements CommandLineRunner {
//    private BookDaoBean bookDaoBean;
//    private UserDaoBean userDaoBean;
//    private NamedParameterJdbcTemplate jdbcTemplate;
//    @Autowired
//    public void setUserDaoBean(UserDaoBean userDaoBean){ this.userDaoBean = userDaoBean;}
//    @Autowired
//    public void setBookDaoBean(BookDaoBean bookDaoBin){
//        this.bookDaoBean = bookDaoBin;
//    }
//    @Autowired
//    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate){
//        this.jdbcTemplate = jdbcTemplate;
//    }


    public static void main(String[] args) throws SQLException {
        SpringApplication.run(Java13SpringSuApplication.class, args);



    }


    @Override
    public void run(String... args) throws Exception {
//        BookDaoJDBC bookDaoJDBC = new BookDaoJDBC();
//        bookDaoJDBC.findBookById(1);
//        System.out.println("---------------------------------------------------------");
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDbApplicationContext.class);
//        BookDaoBin bookDaoBin = ctx.getBean(BookDaoBin.class);
//        System.out.println(bookDaoBin.findBookById(1));
//        System.out.println(bookDaoBean.findBookById(1));
//        System.out.println("---------------------------------------------------------");
//
//
//
//
//        System.out.println("---------------------------------------------------------");
//        List<Book> bookList = jdbcTemplate.query("select * from books_dz6",
//                (res, rowNum) -> new Book(
//                        res.getInt("id"),
//                        res.getString("title"),
//                        res.getString("author"),
//                        res.getDate("date_added")));
//        bookList.forEach(System.out::println);
//        System.out.println(userDaoBean.clientsInfo("+79191257485",UserConst.PHONE_NUMBER));
//        System.out.println(bookDaoBean.bookInfo(userDaoBean.clientsInfo("+79191257485",UserConst.PHONE_NUMBER)));

    }
}
