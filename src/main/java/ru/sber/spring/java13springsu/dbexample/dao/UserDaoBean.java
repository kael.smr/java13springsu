package ru.sber.spring.java13springsu.dbexample.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.dbexample.constants.UserConst;
import ru.sber.spring.java13springsu.dbexample.model.Book;
import ru.sber.spring.java13springsu.dbexample.model.Clients;

import java.sql.*;

@Component
public class UserDaoBean {

    private Connection connection;

//    public UserDaoBean(Connection connection) {
//        this.connection = connection;
//    }
    @Autowired
    public void setConnection(Connection connection){
        this.connection = connection;
    }
    public void addNewUser(Clients newClients) throws SQLException {

        PreparedStatement selectQuery = connection.prepareStatement("insert into" +
                " clients(lastname, firstname, birthday, phone_number, mail, books_list)\n" +
                "values(?,?,?,?,?,?)");
        selectQuery.setString(1,
                newClients.getLastname());
        selectQuery.setString(2,
                newClients.getFirstname());
        selectQuery.setDate(3,
                     newClients.getBirthday());
        selectQuery.setString(4,
                newClients.getPhone_number());
        selectQuery.setString(5,
                newClients.getMail());
        selectQuery.setString(6,
                newClients.getBook_list());
        selectQuery.execute();
    }
    public <T> Clients clientsInfo(T t, UserConst enums) throws SQLException {
        PreparedStatement selectQuery;
        switch (enums) {
            case ID -> {
                selectQuery = connection.prepareStatement("select * from clients where id = ?");
                selectQuery.setInt(1,(Integer)t);
            }
            case MAIL -> {
                selectQuery = connection.prepareStatement("select * from clients where mail = ?");
                selectQuery.setString(1, (String) t);

            } case LASTNAME -> {
                selectQuery =  connection.prepareStatement("select * from clients where lastname = ?");
                selectQuery.setString(1, (String) t);
            }
            case FIRSTNAME -> {
                selectQuery = connection.prepareStatement("select * from clients where firstname = ?");
                selectQuery.setString(1, (String) t);
            }
            case PHONE_NUMBER -> {
                selectQuery = connection.prepareStatement("select * from clients where phone_number = ?");
                selectQuery.setString(1, (String) t);
            }
            case BOOK_LIST -> {
                selectQuery = connection.prepareStatement("select * from clients where books_list = ?");
                selectQuery.setString(1, (String) t);
            }
            case BIRTHDAY -> {
                selectQuery = connection.prepareStatement("select * from clients where birthday = ?");
                selectQuery.setDate(1, (Date) t);
            }
            default -> {
                return new Clients();
            }
        }
        ResultSet result = selectQuery.executeQuery();
        Clients clients = new Clients();
        while (result.next()) {
            clients.setBirthday(result.getDate("birthday"));
            clients.setFirstname(result.getString("firstname"));
            clients.setBook_list(result.getString("books_list"));
            clients.setMail(result.getString("mail"));
            clients.setLastname(result.getString("lastname"));
            clients.setPhone_number(result.getString("phone_number"));
        } return clients;
        }





}

