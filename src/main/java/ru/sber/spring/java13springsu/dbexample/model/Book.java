package ru.sber.spring.java13springsu.dbexample.model;

import lombok.*;

import java.util.Date;
//POJO - Plain OLD Java Object
//@Data
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private Integer bookId;
    private String bookTitle;
    private String author;
    private Date dateAdded;
}
