package ru.sber.spring.java13springsu.dbexample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.dbexample.dao.BookDaoBean;
import ru.sber.spring.java13springsu.dbexample.dao.UserDaoBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.sber.spring.java13springsu.dbexample.constants.DataBaseConsts.*;

@Configuration
@ComponentScan
public class MyDbApplicationContext {
    @Bean
    @Scope("singleton")
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                    "jdbc:postgresql://" + DB_HOST + ":" + PORT +"/" + DB,
                    USER,
                    PASSWORD);

    }
//    @Bean
//    public UserDaoBean userDaoBean() throws SQLException{
//        return new UserDaoBean((getConnection()));
//    }

}
