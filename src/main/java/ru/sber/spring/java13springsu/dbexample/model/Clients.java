package ru.sber.spring.java13springsu.dbexample.model;

import lombok.*;

import javax.xml.crypto.Data;
import java.sql.Date;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class Clients {

    private String lastname;
    private String firstname;
    private Date birthday;
    private String phone_number;
    private String mail;
    private String book_list;



}
