package ru.sber.spring.java13springsu.dbexample.constants;

public enum UserConst {
    ID,
    LASTNAME,
    FIRSTNAME,
    BIRTHDAY,
    PHONE_NUMBER,
    MAIL,
    BOOK_LIST
    //
}