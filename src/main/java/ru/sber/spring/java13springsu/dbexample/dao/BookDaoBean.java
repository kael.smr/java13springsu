package ru.sber.spring.java13springsu.dbexample.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.sber.spring.java13springsu.dbexample.model.Book;
import ru.sber.spring.java13springsu.dbexample.model.Clients;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BookDaoBean {
    private Connection connection;

    @Autowired
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    //    public BookDaoBean(Connection connection){
//        this.connection = connection;
//    }
    public Book findBookById(Integer bookId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from books_dz6 where id = ?");
        selectQuery.setInt(1, bookId);
        ResultSet result = selectQuery.executeQuery();
        Book book = new Book();
        while (result.next()) {
            book.setBookTitle(result.getString("title"));
            book.setAuthor(result.getString("author"));
            book.setDateAdded(result.getDate("date_added"));

        }
        return book;
    }

    public List<Book> bookInfo(Clients clients) throws SQLException {
        List<Book> bookInfo = new ArrayList<>();
        String str = clients.getBook_list();
        String[] array = str.split(", ");
        for (String s : array) {
            PreparedStatement selectQuery = connection.prepareStatement(
                    "select * from books where title =?");
            selectQuery.setString(1, s);
            ResultSet result = selectQuery.executeQuery();
            Book book = new Book();
            while (result.next()) {
                book.setBookTitle(result.getString("title"));
                book.setAuthor(result.getString("author"));
                book.setDateAdded(result.getDate("date_added"));
            }
            bookInfo.add(book);

        }
        return bookInfo;


    }
}
