create table clients(
id bigserial primary key,
lastname varchar(100) not null,
firstname varchar(100) not null,
birthday date not null,
phone_number varchar(150) not null,
mail varchar(150) not null,
books_list varchar(255)
);

-- select * from books;


insert into clients(lastname, firstname, birthday, phone_number, mail, books_list)
values ('Петров','Аркадий', '1989.04.01','+79191257485','petrov@mail.ru','Доктор Живаго, Гибель Богов');

insert into clients(lastname, firstname, birthday, phone_number, mail, books_list)
values ('Пришвин','Владислав', '1991.09.28','+79271277785','vladp@gmail.com','Недоросль');

insert into clients(lastname, firstname, birthday, phone_number, mail, books_list)
values ('Крылова','Екатерина', '1994.08.24','+79379816258','krilovaekt@bk.ru','Сестра моя - жизнь, Гибель Богов');
select * from clients;

