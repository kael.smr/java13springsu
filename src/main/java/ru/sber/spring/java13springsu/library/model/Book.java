package ru.sber.spring.java13springsu.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "books_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@gson_id")
public class Book extends GenericModel{

    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "publish_date", nullable = false)
    private LocalDate publishDate;
    @Column(name = "publish")
    private String publish;
    @Column(name = "page_count")
    private Integer pageCount;

    @Column(name = "storage_place")
    private String storagePlace;

    @Column(name = "online_copy_path")
    private String onlineCopy;
    @Column(name = "description")
    private String description;
    @Column(name = "amount", nullable = false)
    private Integer amount;
    @Column(name = "genre", nullable = false)
    @Enumerated()
    private Genre genre;
    @ManyToMany
//    @JsonBackReference
//    @JsonIgnore
    @JoinTable(name = "books_authors",
            joinColumns =  @JoinColumn(name = "book_id"),
            foreignKey = @ForeignKey(name = "FK_BOOKS_AUTHORS"),
            inverseJoinColumns = @JoinColumn(name = "author_id"),
            inverseForeignKey = @ForeignKey(name = "FK_AUTHOR_BOOKS"))
    private Set<Author> authors;

    @OneToMany(mappedBy = "book")
    private Set<BookRentInfo> bookRentInfos;

}
