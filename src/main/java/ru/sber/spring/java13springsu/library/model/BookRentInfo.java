package ru.sber.spring.java13springsu.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "book_rent_info")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "books_rent_infos_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class BookRentInfo extends GenericModel{

    @ManyToOne
    @JoinColumn(name =  "book_id", foreignKey = @ForeignKey(name = "FK_RENT_BOOK_INFO_BOOKS"))
    private Book book;
    @ManyToOne
    @JoinColumn(name =  "users_id", foreignKey = @ForeignKey(name = "FK_RENT_USER_INFO_BOOKS"))
    private User user;
    @Column(name = "rent_date")
    private LocalDateTime rentDate;

    //полу автоматически должно рассчитываться из rent_date + rent_period
    @Column(name = "return_date")
    private LocalDateTime returnDate;

    //rent_period - количество дней аренды, если не указано, то по-умолчанию - 14 дней.
    @Column(name = "rent_period")
    private Integer rentPeriod;

    @Column(name = "returned")
    private Boolean returned;
}
