package ru.sber.spring.java13springsu.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.java13springsu.library.model.Genre;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO extends GenericDTO{
    private List<BookDTO> bookDTOS;
    private String title;
    private String publishDate;
    private String storagePlace;
    private String publish;
    private Integer pageCount;
    private String onlineCopy;
    private String description;
    private Integer amount;
    private Genre genre;
    private Set<Long> authorsIds;

//    public BookDTO(Book book){
//        BookDTO bookDTO = new BookDTO();
//        bookDTO.setId(bookDTO.getId());
//        bookDTO.setAmount(bookDTO.getAmount());
//        bookDTO.setGenre(bookDTO.getGenre());
//    }
//
//    public BookDTO(List<Book> books){
//        List<BookDTO> bookDTOS = new ArrayList<>();
//        for (Book book: books){
//            BookDTO bookDTO = new BookDTO(book);
//            bookDTOS.add(bookDTO);
//        }
//        this.bookDTOS = bookDTOS;
//    }
}
