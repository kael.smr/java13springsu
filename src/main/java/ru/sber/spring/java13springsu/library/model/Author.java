package ru.sber.spring.java13springsu.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "author")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "authors_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@gson_id")
public class Author extends GenericModel {

    @Column(name = "fio", nullable = false)
    private String fio;
    @Column(name = "description")
    private String description;
    @Column(name = "birth_date")
    private LocalDate birthDate;
//    @ManyToMany(mappedBy = "authors")
//
//    private Set<Book> books;
    @ManyToMany
//    @JsonIgnore
//    @JsonManagedReference
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "author_id"),
                    foreignKey = @ForeignKey(name = "FK_AUTHOR_BOOKS"),
            inverseJoinColumns = @JoinColumn(name = "book_id"),
                    inverseForeignKey = @ForeignKey(name = "FK_BOOK_AUTHORS"))
    private Set<Book> books;
}
