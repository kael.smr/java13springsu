package ru.sber.spring.java13springsu.library.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.library.dto.AuthorDTO;
import ru.sber.spring.java13springsu.library.model.Author;
import ru.sber.spring.java13springsu.library.service.AuthorService;


@RestController
@RequestMapping(value = "/authors")
//http://localhost:9090/books
@Tag(name = "Автор",
        description = "контроллер для работы с авторами книг")
public class AuthorController extends GenericController<Author, AuthorDTO> {


    public AuthorController(AuthorService authorService){
        super(authorService);
    }

//    @Operation(description = "Добавить книгу к автору", method = "addBook")
//    @RequestMapping(value = "/addBook", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Author> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                          @RequestParam(value = "authorId") Long authorId){
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("книга не найдена"));
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("автор не найден"));
//        author.getBooks().add(book);
////        book.getAuthors().add(author);
//        return ResponseEntity.status(HttpStatus.CREATED).body(authorRepository.save(author));
//    }


}
