package ru.sber.spring.java13springsu.library.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.library.dto.BookRentInfoDTO;
import ru.sber.spring.java13springsu.library.model.BookRentInfo;
import ru.sber.spring.java13springsu.library.service.BookRentInfoService;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
        description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class RentBookController extends GenericController<BookRentInfo, BookRentInfoDTO> {
    public RentBookController(BookRentInfoService bookRentInfoService) {
        super(bookRentInfoService);


    }
}
