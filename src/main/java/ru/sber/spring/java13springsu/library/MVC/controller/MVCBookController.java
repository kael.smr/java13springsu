package ru.sber.spring.java13springsu.library.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.spring.java13springsu.library.dto.BookDTO;
import ru.sber.spring.java13springsu.library.dto.BookWithAuthorsDTO;
import ru.sber.spring.java13springsu.library.service.BookService;

import java.util.List;

@Controller
@RequestMapping("books")
public class MVCBookController {

    private final BookService bookService;

    public MVCBookController(BookService bookService){
        this.bookService = bookService;
    }

    @GetMapping("")
    public String getAll(Model model){
        List<BookWithAuthorsDTO> bookDTOList = bookService.getAllBooksWithAuthors();
        model.addAttribute("books", bookDTOList);
        return "books/viewAllBooks";
    }
    //Нарисует форму создания книги
    @GetMapping("/add")
    public String create(){
        return "books/addBook";
    }
    //Примет данные о создаваемой книге
    //Потом вернет нас на страницу со всеми книгами
    @PostMapping("/add")
    public String create(@ModelAttribute("bookForm")BookDTO bookDTO){
        bookService.create(bookDTO);
        return "redirect:/books";
    }
}
