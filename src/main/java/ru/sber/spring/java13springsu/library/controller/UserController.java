package ru.sber.spring.java13springsu.library.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.library.dto.UserDTO;
import ru.sber.spring.java13springsu.library.model.User;
import ru.sber.spring.java13springsu.library.service.UserService;

@RestController
@RequestMapping(name = "/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями библиотеки")
public class UserController extends GenericController<User, UserDTO> {


    public UserController(UserService userService) {
        super(userService);

    }
}
//