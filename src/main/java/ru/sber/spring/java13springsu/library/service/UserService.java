package ru.sber.spring.java13springsu.library.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.library.dto.UserDTO;
import ru.sber.spring.java13springsu.library.mapper.UserMapper;
import ru.sber.spring.java13springsu.library.model.User;
import ru.sber.spring.java13springsu.library.repository.UserRepository;

@Service
public class UserService extends GenericService<User, UserDTO> {
    protected UserService(UserRepository userRepository, UserMapper userMapper){
        super(userRepository, userMapper);
    }
}
