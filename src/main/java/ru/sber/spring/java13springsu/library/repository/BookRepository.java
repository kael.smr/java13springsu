package ru.sber.spring.java13springsu.library.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.library.model.Book;

@Repository
public interface BookRepository extends GenericRepository<Book> {
}
