package ru.sber.spring.java13springsu.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.library.dto.BookRentInfoDTO;
import ru.sber.spring.java13springsu.library.model.BookRentInfo;
import ru.sber.spring.java13springsu.library.repository.BookRepository;
import ru.sber.spring.java13springsu.library.repository.UserRepository;

import java.util.Set;
@Component
public class BookRentInfoMapper extends GenericMapper<BookRentInfo, BookRentInfoDTO>{
    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    protected BookRentInfoMapper(ModelMapper mapper,
                                 BookRepository bookRepository,
                                 UserRepository userRepository) {
        super(mapper, BookRentInfo.class, BookRentInfoDTO.class);
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(BookRentInfo.class, BookRentInfoDTO.class)
                .addMappings(m -> m.skip(BookRentInfoDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookRentInfoDTO::setBookId)).setPostConverter(toDTOConverter());
    }



    @Override
    protected void mapSpecificFields(BookRentInfoDTO source, BookRentInfo destination) {
        destination.setBook(bookRepository.findById(source.getBookId()).orElseThrow(() -> new NotFoundException("Книги не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(BookRentInfo source, BookRentInfoDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setBookId(source.getBook().getId());
    }

    @Override
    protected Set<Long> getIds(BookRentInfo entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

}
