package ru.sber.spring.java13springsu.library.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.library.model.User;
@Repository
public interface UserRepository extends GenericRepository<User> {
}
