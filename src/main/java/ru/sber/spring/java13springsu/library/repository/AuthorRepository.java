package ru.sber.spring.java13springsu.library.repository;


import ru.sber.spring.java13springsu.library.model.Author;

public interface AuthorRepository extends GenericRepository<Author> {
}
