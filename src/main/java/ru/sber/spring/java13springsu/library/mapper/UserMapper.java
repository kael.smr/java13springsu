package ru.sber.spring.java13springsu.library.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.library.dto.UserDTO;
import ru.sber.spring.java13springsu.library.model.User;

import java.util.Set;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {
    protected UserMapper(ModelMapper modelMapper) {
        super(modelMapper, User.class, UserDTO.class);
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

    @Override
    protected Set<Long> getIds(User entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

}
