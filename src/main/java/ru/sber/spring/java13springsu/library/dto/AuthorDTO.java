package ru.sber.spring.java13springsu.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class AuthorDTO extends GenericDTO{

    private String fio;

    private String description;

    private LocalDate birthDate;

    private Set<Long> booksIds;
}
