package ru.sber.spring.java13springsu.library.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.library.model.BookRentInfo;
@Repository
public interface BookRentInfoRepository extends GenericRepository<BookRentInfo> {
}
