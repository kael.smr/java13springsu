package ru.sber.spring.java13springsu.library.model;

public enum Genre {

    FANTASY("Фантастика"),
    DRAMA("Драма"),
    SCIENCE_FICTION("Научная фантастика"),
    NOVEL("Роман");

    private final String genreDisplayValue;

    Genre(String text){
        this.genreDisplayValue = text;
    }
    public String getGenreDisplayValue(){
        return this.genreDisplayValue;
    }
}
