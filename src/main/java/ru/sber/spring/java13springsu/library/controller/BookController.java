package ru.sber.spring.java13springsu.library.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.library.dto.BookDTO;
import ru.sber.spring.java13springsu.library.model.Book;
import ru.sber.spring.java13springsu.library.service.BookService;


@RestController
@RequestMapping(value = "/books")
//http://localhost:9090/books
@Tag(name = "книги",
description = "контроллер для работы с книгами библиотеки")
public class BookController extends GenericController<Book, BookDTO>{



    public BookController(BookService bookService){
        super(bookService);
        ;
    }
    //http://localhost:9090/api/rest/swagger-ui/index.html#/ - подключение swagger'а
    //http://localhost:9009/api/rest/books/getById?id=1
//    @Operation(description = "Добавить автора к книге", method = "addAuthor")
//    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST,
//    produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                          @RequestParam(value = "authorId") Long authorId){
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("книга не найдена"));
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("автор не найден"));
//        book.getAuthors().add(author);
//        return ResponseEntity.status(HttpStatus.CREATED).body(bookRepository.save(book));
//    }

}
