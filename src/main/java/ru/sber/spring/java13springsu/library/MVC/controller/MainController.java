package ru.sber.spring.java13springsu.library.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    //http://localhost:9090/
    @GetMapping("/")
    public String index(){
        return "index";
    }
}
