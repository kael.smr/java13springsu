package ru.sber.spring.java13springsu.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.library.dto.BookDTO;
import ru.sber.spring.java13springsu.library.model.Book;
import ru.sber.spring.java13springsu.library.model.GenericModel;
import ru.sber.spring.java13springsu.library.repository.AuthorRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookMapper extends GenericMapper<Book, BookDTO>  {

    private final AuthorRepository authorRepository;

//    private final ModelMapper modelMapper;
    protected BookMapper(ModelMapper modelMapper, AuthorRepository authorRepository){
        super(modelMapper,Book.class, BookDTO.class);
        this.authorRepository = authorRepository;
//        this.modelMapper = modelMapper;
    }
    @PostConstruct
    public void setupMapper(){
        modelMapper.createTypeMap(Book.class,BookDTO.class)
                .addMappings(m -> m.skip(BookDTO::setAuthorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BookDTO.class,Book.class)
                .addMappings(m ->m.skip(Book::setAuthors)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Book::setPublishDate)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(BookDTO source, Book destination) {
        if (!Objects.isNull(source.getAuthorsIds())) {
            destination.setAuthors(new HashSet<>(authorRepository.findAllById(source.getAuthorsIds())));
        } else {
            destination.setAuthors(Collections.emptySet());
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(source.getPublishDate(), formatter);
        destination.setPublishDate(date);
    }

    @Override
    protected void mapSpecificFields(Book source, BookDTO destination) {
        destination.setAuthorsIds(getIds(source));
    }
    @Override
    protected Set<Long> getIds(Book entity){
        return Objects.isNull(entity) || Objects.isNull(entity.getAuthors())
                ? null
                :entity.getAuthors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
