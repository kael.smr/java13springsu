package ru.sber.spring.java13springsu.library.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.library.dto.BookDTO;
import ru.sber.spring.java13springsu.library.dto.BookWithAuthorsDTO;
import ru.sber.spring.java13springsu.library.mapper.BookMapper;
import ru.sber.spring.java13springsu.library.mapper.BookWithAuthorsMapper;
import ru.sber.spring.java13springsu.library.model.Book;
import ru.sber.spring.java13springsu.library.repository.BookRepository;

import java.util.List;

@Service
public class BookService
        extends GenericService<Book,BookDTO> {
    private final BookWithAuthorsMapper bookWithAuthorsMapper;
    private final BookRepository bookRepository;



    protected BookService(BookRepository bookRepository,
                          BookMapper bookMapper,
                          BookWithAuthorsMapper bookWithAuthorsMapper){
        super(bookRepository,bookMapper);
        this.bookWithAuthorsMapper = bookWithAuthorsMapper;
        this.bookRepository = bookRepository;
    }

    public List<BookWithAuthorsDTO> getAllBooksWithAuthors() {
        return bookWithAuthorsMapper.toDTOs(bookRepository.findAll());
    }

//    public BookDTO getOne(final long id) {
//        return new BookDTO(bookRepository.findById(id).orElseThrow(() -> new NotFoundException("")));
//    }
}
