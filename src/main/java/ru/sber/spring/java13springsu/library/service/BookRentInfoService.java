package ru.sber.spring.java13springsu.library.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.library.dto.BookRentInfoDTO;
import ru.sber.spring.java13springsu.library.mapper.BookRentInfoMapper;
import ru.sber.spring.java13springsu.library.model.BookRentInfo;
import ru.sber.spring.java13springsu.library.repository.BookRentInfoRepository;

@Service
public class BookRentInfoService extends GenericService<BookRentInfo, BookRentInfoDTO> {

    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository, BookRentInfoMapper bookRentInfoMapper){
        super(bookRentInfoRepository,bookRentInfoMapper);
    }
}
