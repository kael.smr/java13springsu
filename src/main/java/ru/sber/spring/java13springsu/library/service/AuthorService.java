package ru.sber.spring.java13springsu.library.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.library.dto.AuthorDTO;
import ru.sber.spring.java13springsu.library.mapper.AuthorMapper;
import ru.sber.spring.java13springsu.library.model.Author;
import ru.sber.spring.java13springsu.library.repository.AuthorRepository;

@Service
public class AuthorService extends GenericService<Author, AuthorDTO> {
    protected AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper){
        super(authorRepository,authorMapper);
    }
}
